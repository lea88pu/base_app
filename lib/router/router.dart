import 'package:base_app/ui/page/home_page.dart';
import 'package:base_app/ui/page/profile_page.dart';
import 'package:base_app/ui/page/splash_screen_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/navigator.dart';
import 'package:base_app/ui/page/login_page.dart';

class Router {
  static Route onGenerateRoute(RouteSettings settings) {
    var routeName = settings.name;
    switch (routeName) {
      case Routes.home:
        print ("home");
        return MaterialPageRoute(builder: (context) => MyHomePage());
      case Routes.profile:
        print ("profile");
        return MaterialPageRoute(builder: (context) => ProfilePage());
      case Routes.login:
        print ("login");
        return MaterialPageRoute(builder: (context) => LoginPage());
      case Routes.splash:
        print ("splash");
        return MaterialPageRoute(builder: (context) => SplashScreen());

      default:
        return MaterialPageRoute(
            builder: (context) => Center(
                  child: Text('route missing'),
                ));
    }
  }
}

class Routes {
  static const String home = '/';
  static const String profile = '/profile';

  static const String splash = 'splash';
  static const String login = 'login';
}
