import 'package:base_app/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserAvatar extends StatelessWidget {
  final double radius;

  UserAvatar(this.radius);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'userAvatar',
      child: Consumer<UserVO>(
        builder: (_, vo, __) => CircleAvatar(

          radius: radius,
          backgroundImage: vo.profilePicture != null
              ? Image.network(vo.profilePicture).image
              : null,
          child: vo.profilePicture == null
              ? Text(
                  vo.name[0].toUpperCase() + vo.surname[0].toUpperCase(),
                  style: Theme.of(context).textTheme.headline,
                )
              : null,

        ),
      ),
    );
  }
}
