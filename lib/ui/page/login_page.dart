import 'package:base_app/model/text_model.dart';
import 'package:base_app/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:base_app/router/router.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final customPink = 0xFFF48FB1;

  final TextEditingController _username = TextEditingController();

  final TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(customPink),
        title: Text(
          "Login Page",
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20.0),
            ),
            Container(
              padding: EdgeInsets.all(20.0),
              color: Colors.grey,
            ),
            SingleChildScrollView(
              padding: EdgeInsets.all(20.0),
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(
                      height: 100.0,
                    ),
                    Text(
                      "Welcome",
                      style: TextStyle(
                        color: Color(customPink),
                        fontSize: 35.0,
                        fontFamily: 'Kurale',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Log in to continue",
                      style: TextStyle(
                          fontFamily: 'Kurale',
                          color: Colors.white,
                          fontSize: 20.0),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),

                    TextField(
                      controller: _username,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          hintText: "Username",
                          hintStyle: TextStyle(
                            color: Color(customPink),
                            fontFamily: 'Kurale',
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white54))),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextField(
                      controller: _password,
                      obscureText: true,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          hintText: "Password",
                          hintStyle: TextStyle(
                            color: Color(customPink),
                            fontFamily: 'Kurale',
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white54))),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Forgot your password?",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Kurale',
                            fontStyle: FontStyle.italic),
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        child: Text(
                          "Login".toUpperCase(),
                          style: TextStyle(fontFamily: 'Kurale'),
                        ),
                        onPressed: () async {
                          // dodajemo dialogue na stack
                          showDialog(context: context,
                              barrierDismissible: false,
                              builder: (context) =>
                                  Center(child: CircularProgressIndicator(),));
                          LoginEmail loginEmail = Provider.of(context);
                          try {
                            await loginEmail(_username.text,_password.text);
                          } catch (e) {
                            print(e);
                            Navigator.pop(context); // popamo dialogue
                            return null;
                          }
                          Navigator.pop(context); // popamo dialogue
                          return Navigator.pushReplacementNamed(
                              context, Routes.home); // i onda replacement
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
