import 'package:base_app/model/counter_model.dart';
import 'package:base_app/model/text_model.dart';
import 'package:base_app/router/router.dart';
import 'package:base_app/ui/atom/user_avatar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var customPink = 0xFFF48FB1;

  var _controller = TextEditingController();

  var _isActive = false;

  @override
  Widget build(BuildContext context) {
    var action = Provider.of<OnUpdateTextFieldAction>(context);

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(customPink),

        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Counter'),
        actions: <Widget>[
          InkWell(
            onTap: () => Navigator.pushNamed(context, Routes.profile),
           child: UserAvatar(24),
         ),
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            TextField(
              onChanged: (text) {
                setState(() {
                  print('on changed $text');
                  _isActive = true;
                  action(text);
                });
              },
              controller: _controller,
            ),
            _isActive
                ? Expanded(
              child: Consumer<AutoCompleteVO>(
                builder: (_, AutoCompleteVO vo, __) {
                  print(vo);
                  return ListView.builder(
                    itemBuilder: (context, i) => ListTile(
                      onTap: () {
                        setState(() {
                          _isActive = false;
                          _controller.text = vo.completes[i];
                        });
                      },
                      title: Text(vo.completes[i]),
                    ),
                    itemCount: vo.completes.length,
                  );
                },
              ),
            )
                : Container(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: Provider.of<IncreaseCounterAction>(context),
        tooltip: 'Increment',
        child: Icon(Icons.add),
        backgroundColor: Color(customPink),
      ), // This
      floatingActionButtonLocation: FloatingActionButtonLocation
          .centerFloat, // trailing comma makes auto-formatting nicer for build methods.
    );
  }
}