import 'package:base_app/model/user_model.dart';
import 'package:base_app/router/router.dart';
import 'package:base_app/ui/atom/user_avatar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatelessWidget {
  final customPink = 0xFFF48FB1;
  final loginButton = "Logout";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(customPink),
        title: Text("Profile"),
      ),
      body: Consumer<UserVO>(
        builder: (_, vo, __) => Center(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                stops: [0.1, 0.3, 0.5, 0.7],
                colors: [
                  Colors.grey,
                  Colors.grey,
                  Color(customPink),
                  Color(customPink),
                ],
              ),
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: UserAvatar(100),
                ),

                Text("Welcome ",
                    style: TextStyle(
                        fontFamily: 'Kurale',
                        fontSize: 35,
                        fontWeight: FontWeight.bold)),

                Text("${vo.name} ${vo.surname}",
                    style: TextStyle(
                        fontFamily: 'Kurale',
                        fontSize: 25,
                        fontStyle: FontStyle.italic)),

                Card(
                  margin: EdgeInsets.only(top: 25.0),
                  elevation: 8,
                  child: Container(
                    height: 50,
                    width: 300,
                    child: Center(
                      child: Text(
                        "email: ${vo.email}",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'Kurale',
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Card(
                  margin: EdgeInsets.only(top: 20.0),
                  elevation: 8,
                  child: Container(
                    height: 50,
                    width: 300,
                    child: Center(
                      child: Text(
                        "phone: ${vo.phone}",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'Kurale',
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                OutlineButton(
                    child: Text(loginButton),
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          'login', (Route<dynamic> route) => true);
                    })
                //welcome $name $surname
              ],
            ),
          ),
        ),
      ),
    );
  }
}
