import 'package:base_app/model/user_model.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:base_app/router/router.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  var customPink = 0xFFF48FB1;

  Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer(Duration(seconds: 3), () {
      var userLogInVO = Provider.of<UserLogInVO>(context, listen: false);
      if (userLogInVO.login == false) {
        Navigator.pushReplacementNamed(context, Routes.login);
      } else {
        Navigator.pushReplacementNamed(context, Routes.home);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (timer.isActive) {
      timer.cancel();
    }
    timer = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 100,
          height: 100,
          child: CircularProgressIndicator(
            backgroundColor: Color(customPink),
          ),
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
}
