import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

typedef Future<void> LoginEmail(String email, String password);

class UserModel {
  // Observable
  var userLogin$ = BehaviorSubject<UserLogInVO>()
    ..add(UserLogInVO(login: false));

  // Observable
  var user$ = BehaviorSubject<UserVO>();

  // Action
  Future<void> loginUserA(String email, String password) async {
    print(password);
    user$.add(
      UserVO(
          name: "Ivana",
          surname: "Petrof",
          email: email,
          phone: "+359875465887",
          profilePicture:
              'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/colorful-of-dahlia-pink-flower-in-beautiful-garden-royalty-free-image-825886130-1554743243.jpg?crop=0.669xw:1.00xh;0.331xw,0&resize=640:*'),
    );
    userLogin$.add(UserLogInVO(login: true));
  }

  void dispose() {
    userLogin$.close();
    user$.close();
  }

  static List<SingleChildCloneableWidget> get provide {
    var model = UserModel();

    return [
      //singleton
      Provider<UserModel>(
        builder: (context) => model,
        dispose: (context, model) => model.dispose(),
      ),
      //observable
      StreamProvider<UserVO>.value(
        value: model.user$,
      ),
      StreamProvider<UserLogInVO>.value(
        value: model.userLogin$,
      ),
      Provider<LoginEmail>.value(value: model.loginUserA)
    ];
  }
}

class UserLogInVO {
  final bool login;
  final num id;

  UserLogInVO({this.login, this.id});
}

class UserVO {
  final String name;
  final String surname;
  final String email;
  final String phone;
  final String profilePicture;

  UserVO(
      {@required this.name,
      @required this.surname,
      this.email,
      this.phone,
      this.profilePicture});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserVO &&
          runtimeType == other.runtimeType &&
          profilePicture == other.profilePicture &&
          name == other.name &&
          surname == other.surname &&
          email == other.email &&
          phone == other.phone;

  @override
  int get hashCode =>
      profilePicture.hashCode ^
      name.hashCode ^
      surname.hashCode ^
      email.hashCode ^
      phone.hashCode;
}
