import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

typedef void IncreaseCounterAction();

class CounterModel {
  var counter$ = ValueNotifier<CounterVO>(CounterVO(0));

  void increaseCounterAction() {
    counter$.value = CounterVO(counter$.value.counter + 1);
  }

  static List<SingleChildCloneableWidget> get provide {
    var counterModel = CounterModel();
    return [
      // map singleton
      Provider<CounterModel>.value(
        value: counterModel,
        updateShouldNotify: (_, __) => false,
      ),
      // map observables
      ValueListenableProvider<CounterVO>(
        builder: (context) => counterModel.counter$,
      ),
      // map actions
      Provider<IncreaseCounterAction>.value(
        value: counterModel.increaseCounterAction,
        updateShouldNotify: (_, __) => false,
      )
    ];
  }
}

class CounterVO {
  final int counter;
  CounterVO(this.counter);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is CounterVO &&
              runtimeType == other.runtimeType &&
              counter == other.counter;

  @override
  int get hashCode => counter.hashCode;
}
