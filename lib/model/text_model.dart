import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

typedef void OnUpdateTextFieldAction(String text);

class TextModel {
  PublishSubject<AutoCompleteVO> autoComplete$ =
  PublishSubject<AutoCompleteVO>();

  void dispose() {
    autoComplete$.close();
  }

  void onUpdateTextFieldAction(String text) {
    List<String> listOfFilters = database.where((el) {
      return el.startsWith(text);
    }).toList();

    autoComplete$.add(AutoCompleteVO(
        completes: listOfFilters.length > 5
            ? listOfFilters.sublist(0, 5)
            : listOfFilters));
  }

  List<String> database = ['one', 'two','three','four','five','six','seven','eight','nine','ten'
  ];


  static List<SingleChildCloneableWidget> get provide {
    var model = TextModel();
    return [
      Provider<TextModel>.value(value: model),

      StreamProvider<AutoCompleteVO>.value(value: model.autoComplete$),
      Provider<OnUpdateTextFieldAction>.value(value: model.onUpdateTextFieldAction),
    ];
  }
}

class AutoCompleteVO {
  final List<String> completes;

  const AutoCompleteVO({
    @required this.completes,
  });
}